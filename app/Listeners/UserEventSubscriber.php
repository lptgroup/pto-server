<?php

namespace App\Listeners;

use App\Notifications\UserActivated;

class UserEventSubscriber
{
    /**
     * Handle user login events.
     */
    public function onUserLogin($event) {}

    /**
     * Handle user login events.
     */
    public function onUserActivation($event) {
    		$admin = \App\User::first();
    	  $admin->notify(new UserActivated($event->user));
    }

    /**
     * Handle user logout events.
     */
    public function onUserLogout($event) {}

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\UserHasActivatedAccount',
            'App\Listeners\UserEventSubscriber@onUserActivation'
        );

        // $events->listen(
        //     'Illuminate\Auth\Events\Login',
        //     'App\Listeners\UserEventSubscriber@onUserLogin'
        // );

        // $events->listen(
        //     'Illuminate\Auth\Events\Logout',
        //     'App\Listeners\UserEventSubscriber@onUserLogout'
        // );
    }

}