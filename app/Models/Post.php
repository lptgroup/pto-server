<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Post extends Model
{
    use Searchable;

    protected $fillable = [
        'user_id',
        'title',
        'slug',
        'body'
    ];

    public function author()
    {
      return $this->belongsTo('App\User', 'user_id');
    }

    //fields you want to make searchable
    public function toSearchableArray()
    {
        $array = $this->toArray('title');

        // Customize array...

        return $array;
    }

}
