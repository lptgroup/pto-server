<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
      'first_name',
      'last_name',
      'phone',
      'pwz',
      'dob'
    ];

    protected $hidden = [
        'id', 'user_id', 'created_at', 'updated_at'
    ];

    /**
     * user() one-to-one relationship method
     *
     * @return QueryBuilder
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
