<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'activated'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function delete(){
        $this->profile->delete();
        parent::delete();
    }

    public function update(array $attributes = [], array $options = []) {
        if (! $this->exists) {
            return false;
        }
        if(!$this->profile) {
            $this->profile()->create(array_only($attributes, [
                'first_name',
                'last_name',
                'phone',
                'pwz',
                'dob'])
            );
        } else {
            $this->profile()->update(array_only($attributes, [
                'first_name',
                'last_name',
                'phone',
                'pwz',
                'dob'])
            );
        }
        parent::update($attributes, $options);
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }
}
