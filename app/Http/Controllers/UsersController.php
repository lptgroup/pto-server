<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use App\User;
use Carbon\Carbon;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use App\Http\Requests\UpdateProfilePicture;
use App\Events\UserHasRegistered;
use App\Services\EloquentVueTables;

class UsersController extends Controller
{
    public function index(Request $request) {
    	$response = new EloquentVueTables();

      return $response->get('users', $request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::with('profile')->findOrFail($id);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function authUser(Guard $auth)
    {
      $auth->user()->profile = $auth->user()->profile; 

      return $auth->user();
    }

  	/**
  	 * Store a newly created resource in storage.
  	 *
  	 * @param  \App\Http\Requests\StorePost  $request
  	 * @return \Illuminate\Http\Response
  	 */
  	public function updateProfilePicture($id, UpdateProfilePicture $request)
  	{
      $path = $request->file('avatar')->store('avatars', 'public');

      return $path;
  	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Guard $auth, StoreUser $request)
    {
        $input = $request->all();

        $input['password'] = bcrypt($input['password']);

        return User::create($input);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePost  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, UpdateUser $request)
    {
      $user = User::findOrFail($id);

      return $user->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $entry = User::findOrFail($id);
      
      $entry->delete();
    }

}
