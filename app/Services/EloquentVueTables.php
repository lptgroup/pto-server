<?php

namespace App\Services;

use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

Class EloquentVueTables {

  public function get($table, Array $input) {
    $data = DB::table($table);
    $data->select( array_keys($input['query']) );
    if (isset($input['query']) && $input['query']) {
	     $data = $input['byColumn'] == 1 ? $this->filterByColumn($data, $input['query']):
	     $this->filter($data, $input['query'], array_keys($input['query']));
		}
  	$count = $data->count();
  	$data->limit($input['limit'])->skip($input['limit'] * ($input['page']-1));
	  if (isset($input['orderBy']) && $input['orderBy']){
	  	  $direction = $input['ascending'] == 1? "ASC" : "DESC";
	  		$data->orderBy($input['orderBy'], $direction);
	  }
	  return [
			'data'=>$data->get(),
	    'count'=>$count
	  ];
	}

	protected function filterByColumn($data, $query) {
  	foreach ($query as $field=>$query){
    	if (!$query) continue;
  		if (is_string($query)) {
	   		$data->where($field,'LIKE',"%{$query}%");
	 		} else {
			  $start = Carbon::createFromFormat('Y-m-d',$query['start'])->startOfDay();
			  $end = Carbon::createFromFormat('Y-m-d',$query['end'])->endOfDay();
			  $data->whereBetween($field,[$start, $end]);
			}
		}
		return $data;
	}

	protected function filter($data, $query, $fields) {
  	foreach ($fields as $index=>$field):
    	$method = $index?"orWhere":"where";
  		$data->{$method}($field,'LIKE',"%{$query}%");
  	endforeach;
	  return $data;
	}
}