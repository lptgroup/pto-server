@extends('layouts.app') 
@section('content')
<div class="container">
  <div class="row">
    <div class="col-xs-12">
    	@if(count(Auth::user()->unreadNotifications))
	  		<pre>{{Auth::user()->unreadNotifications}}</pre>
	    	
	    	@foreach(Auth::user()->unreadNotifications as $notification)
	    		@include('notifications.' . snake_case(class_basename($notification->type)))
	    	@endforeach

			<!-- AJAX -->
<!-- 
			<form method="DELETE" action="/users/{{ Auth::id() }}/notifications">
				<button type="submit">Mark as read</button>
			</form>
 -->
				<form method="POST" action="/users/{{ Auth::id() }}/notifications">
					{{ method_field('DELETE') }}
					{{ csrf_field() }}
					<button type="submit">Mark as read</button>
				</form>
			@endif     
   		<passport-clients></passport-clients>
    	<passport-authorized-clients></passport-authorized-clients>
    	<passport-personal-access-tokens></passport-personal-access-tokens>
    </div>
  </div>
</div>
@endsection
