@extends('layouts.redis')

@section('content')
    <p id="power">0</p>
@stop

<ul id="chat"></ul>

@section('footer')
    <script src="//cdnjs.cloudflare.com/ajax/libs/socket.io/1.5.0/socket.io.js"></script>
    <script>
        var socket = io('http://socket.refresh.lptgroup.pl');
        // var socket = io('http://192.168.10.10:3000');
        socket.on("test-channel:App\\Events\\EventName", function(message){
            // increase the power everytime we load test route
            $('#power').text(parseInt($('#power').text()) + parseInt(message.data.power));
        });
        socket.on("chat-room.1:App\\Events\\ChatMessageWasReceived", function(message){
            $('#chat').append('<li>' + message.user.email + ': ' + message.chatMessage.message + '</li>')
           
        });
    </script>
@stop