<?php

use App\Notifications\PostPublished;
use App\Notifications\SeriesUpdated;
use App\Events\UserHasRegistered;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/redis/fire', function () {
    // this fires the event
    event(new App\Events\EventName());
    return "event fired";
});

Route::get('/redis/test', function () {
    // this checks for the event
    return view('test');
});

Route::get('/mail', function () {
	$user = App\User::first();
	$post = App\Models\Post::first();
  $user->notify(new PostPublished($post));
});

Route::get('/series-updated', function () {
	$user = App\User::first();
	$post = App\Models\Post::first();
  $user->notify(new SeriesUpdated($post));
});

Route::get('/broadcast', function () {
	$user = App\User::first();
  event(new UserHasRegistered('Dupa Cycki'));
});

Route::delete('/users/{user}/notifications', function (\App\User $user) {
	$user->unreadNotifications->map( function($n) {
		$n->markAsRead();
	});

	return back();
});




Auth::routes();
Route::get('user/activation/{token}', 'Auth\RegisterController@activateUser')
	->name('user.activate');

Route::get('/home', 'HomeController@index');
Route::resource('posts', 'PostsController');
