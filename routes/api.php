<?php

use Illuminate\Http\Request;
use App\Services\VueTables\EloquentVueTables;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users/mea', function () {
	return Auth::user(function($query){
      $query->with('profile');
  });
});

Route::post('/users/{id}/updateProfilePicture', [
	'uses' => 'UsersController@updateProfilePicture'
])->middleware('scope:conference,dupa');

Route::get('/users/mma', [
	'uses' => 'UsersController@authUser'
]);

Route::get('/users/{id}', [
	'uses' => 'UsersController@show'
])->middleware('scope:conference,dupa');

Route::get('/users', [
	'uses' => 'UsersController@index'
]);

Route::post('/users', [
	'uses' => 'UsersController@store'
])->middleware('scope:conference,dupa');

Route::delete('/users/{id}', [
	'uses' => 'UsersController@destroy'
])->middleware('scope:conference,dupa');

Route::put('/users/{id}', [
	'uses' => 'UsersController@update'
])->middleware('scope:conference,dupa');

Route::get('/posts', [
	'uses' => 'PostsController@index'
])->middleware('scope:conference,dupa');

Route::get('/unreadNotifications', function () {
	return Auth::user()->unreadNotifications;
});

Route::resource('posts', 'PostsController');
Route::get('/whatever', function () {
    echo "dupa";
});